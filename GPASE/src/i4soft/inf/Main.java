package i4soft.inf;

import i4soft.inf.gp.MetricsGP;
import i4soft.inf.ga.MetricsGA;
import i4soft.inf.gp.MetricsGP2;
import i4soft.inf.gp.MetricsGP3;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length >= 3) {
            for (int i = 1; i <= 7; i++) {
                for (int j = 1; j <= 3; j++) {
                    runGP3(Integer.parseInt(args[0]), Integer.parseInt(args[1]),
                            Integer.parseInt(args[2]), i, j);
                }
            }
        } else {
            System.out.println("Parameters: runs, population, generations.");
        }
    }

    public static void runGA(int Runs, int pop, int gen, int prog, int set) throws Exception {
        ArrayList<ArrayList<Double>> sumResults = MetricsGA.exec(1, pop, gen, prog, set);

        ArrayList<ArrayList<Double>> results;

        for (int i = 2; i <= Runs; i++) {
            results = MetricsGA.exec(i, pop, gen, prog, set);
            for (int j = 0; j < results.size(); j++) {
                for (int j2 = 0; j2 < results.get(j).size(); j2++) {
                    sumResults.get(j).set(j2, sumResults.get(j).get(j2) + results.get(j).get(j2));
                }
            }
        }


        for (int j = 0; j < sumResults.size(); j++) {
            for (int j2 = 0; j2 < sumResults.get(j).size(); j2++) {
                sumResults.get(j).set(j2, sumResults.get(j).get(j2)/Runs);
            }
        }

        System.out.println(sumResults.toString());
    }

    public static void runGP(int Runs, int pop, int gen, int prog, int set) throws Exception {
        ArrayList<ArrayList<Double>> sumResults = MetricsGP.exec(1, pop, gen, prog, set);

        ArrayList<ArrayList<Double>> results;

        for (int i = 2; i <= Runs; i++) {
            results = MetricsGP.exec(i, pop, gen, prog, set);
            for (int j = 0; j < results.size(); j++) {
                for (int j2 = 0; j2 < results.get(j).size(); j2++) {
                    sumResults.get(j).set(j2, sumResults.get(j).get(j2) + results.get(j).get(j2));
                }
            }
        }


        for (int j = 0; j < sumResults.size(); j++) {
            for (int j2 = 0; j2 < sumResults.get(j).size(); j2++) {
                sumResults.get(j).set(j2, sumResults.get(j).get(j2)/Runs);
            }
        }

        System.out.println(sumResults.toString());
    }

    public static void runGP2(int Runs, int pop, int gen, int prog, int set) throws Exception {
        ArrayList<ArrayList<Double>> sumResults = MetricsGP2.exec(1, pop, gen, prog, set);

        ArrayList<ArrayList<Double>> results;

        for (int i = 2; i <= Runs; i++) {
            results = MetricsGP2.exec(i, pop, gen, prog, set);
            for (int j = 0; j < results.size(); j++) {
                for (int j2 = 0; j2 < results.get(j).size(); j2++) {
                    sumResults.get(j).set(j2, sumResults.get(j).get(j2) + results.get(j).get(j2));
                }
            }
        }


        for (int j = 0; j < sumResults.size(); j++) {
            for (int j2 = 0; j2 < sumResults.get(j).size(); j2++) {
                sumResults.get(j).set(j2, sumResults.get(j).get(j2)/Runs);
            }
        }

        System.out.println(sumResults.toString());
    }

    public static void runGP3(int Runs, int pop, int gen, int prog, int set) throws Exception {
        ArrayList<ArrayList<Double>> sumResults = MetricsGP3.exec(1, pop, gen, prog, set);

        ArrayList<ArrayList<Double>> results;

        for (int i = 2; i <= Runs; i++) {
            results = MetricsGP3.exec(i, pop, gen, prog, set);
            for (int j = 0; j < results.size(); j++) {
                for (int j2 = 0; j2 < results.get(j).size(); j2++) {
                    sumResults.get(j).set(j2, sumResults.get(j).get(j2) + results.get(j).get(j2));
                }
            }
        }


        for (int j = 0; j < sumResults.size(); j++) {
            for (int j2 = 0; j2 < sumResults.get(j).size(); j2++) {
                sumResults.get(j).set(j2, sumResults.get(j).get(j2)/Runs);
            }
        }

        System.out.println(sumResults.toString());
    }
}
