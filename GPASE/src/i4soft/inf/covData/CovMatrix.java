package i4soft.inf.covData;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CovMatrix {
	private int [][] covMatFailed;
	private int [][] covMatSuccessful;
	private int [] qtdFailed;//quantidade de casos de teste negativos que cobrem cada elemento
	private int [] qtdSuccessful;//quantidade de casos de teste positivos que cobrem cada elemento
	private int tests;//numero de casos de teste
	private int sTests, fTests;//numero de casos de teste positivos e negativos
	private int elements;//n de linhas instrumentalizadas no programa
	private ArrayList<ArrayList<Integer>> children;//quais linhas sao blocos de controle (if, while, for)
	private Element [] Elements;
	
	public CovMatrix(String covMatFile, String failedTCFile, String childrenFileName, int tests, int elements) {
		int [][] covMat = readCovMatCSV(covMatFile, tests, elements);
		int [] failedTC = readFailedCSV(failedTCFile);
		
		this.tests = tests;
		this.fTests = failedTC.length;
		this.sTests = tests - fTests;
		this.elements = elements;
		this.covMatFailed = getFailed(covMat, failedTC);
		this.covMatSuccessful = getSuccessful(covMat, failedTC);
		this.qtdFailed = getFailedN();
		this.qtdSuccessful = getSuccessfulN();
		this.children = readChildrenCSV(childrenFileName);
		this.Elements = getElements();
		
	}

	public CovMatrix(String covMatFile, String failedTCFile, int tests, int elements) {
		int [][] covMat = readCovMatCSV(covMatFile, tests, elements);
		int [] failedTC = readFailedCSV(failedTCFile);

		this.tests = tests;
		this.fTests = failedTC.length;
		this.sTests = tests - fTests;
		this.elements = elements;
		this.covMatFailed = getFailed(covMat, failedTC);
		this.covMatSuccessful = getSuccessful(covMat, failedTC);
		this.qtdFailed = getFailedN();
		this.qtdSuccessful = getSuccessfulN();
		this.Elements = getElements();

	}


	private static int[][] readCovMatCSV(String FileName, int lines, int colums) {
		int[][] csvMatrix = new int[lines][colums];
		
        BufferedReader br = null;
        String line = "";

        try {

            br = new BufferedReader(new FileReader(FileName));
            
            int i = 0;//contador para linhas
            while ((line = br.readLine()) != null) {

            	String[] Line = line.split(",");

            	for (int j = 0; j < Line.length; j++) {
                	csvMatrix[i][j] = Integer.parseInt(Line[j]);
				}
                
                i++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return csvMatrix;
	}
	
	private static int[] readFailedCSV(String FileName) {
		int[] csvMatrix = new int[1];
		
        BufferedReader br = null;
        String line = "";

        try {

            br = new BufferedReader(new FileReader(FileName));
            
            while ((line = br.readLine()) != null) {
            	
            	String[] Line = line.split(",");
            	csvMatrix = new int[Line.length];
            	
                for (int j = 0; j < Line.length; j++) {
                	csvMatrix[j] = Integer.parseInt(Line[j]);
				}

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return csvMatrix;
	}
	
	//leitura do arquivo de filhos de blocos de controle
	private ArrayList<ArrayList<Integer>> readChildrenCSV(String FileName) {
		ArrayList<ArrayList<Integer>> children = new ArrayList<ArrayList<Integer>>();
		
        BufferedReader br = null;
        String line = "";

        try {

            br = new BufferedReader(new FileReader(FileName));
            
            int i = 0;//contador de linhas do arquivo
            while ((line = br.readLine()) != null) {
            	        		
        		children.add(new ArrayList<Integer>());
            	
        		if (!line.equals("")) {
        			String[] Line = line.split(",");
                	for (int j = 0; j < Line.length; j++) {
                    	children.get(i).add(Integer.parseInt(Line[j]));
    				}
                }
            	
            	i++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return children;
	}

	//retorna matriz de cobertura para os casos de teste negativos
	private int[][] getFailed(int [][] covMat, int [] failedTC) {
		//int nFailed = failedTC.length;//numero de casos de teste negativos
		int [][] Failed = new int[fTests][elements];
		
		int i =0;
		for (int j = 0; j < tests; j++) {
			if (j == failedTC[i]) {
				for (int k = 0; k < elements; k++) {
					Failed[i][k] = covMat[j][k];
				}
				i++;
				if (i == fTests) {
					break;
				}
			}
		}
		
		return Failed;
	}
	
	//retorna matriz de cobertura para os casos de teste negativos
	private int[][] getSuccessful(int [][] covMat, int [] failedTC) {
		//int nFailed = failedTC.length;//numero de casos de teste negativos
		int [][] Failed = new int[sTests][elements];
		
		int i =0;
		for (int j = 0; j < tests; j++) {
			if (i == fTests) {
				for (int k = 0; k < elements; k++) {
					Failed[j-i][k] = covMat[j][k];
				}
			} else {
				if (j != failedTC[i]) {
					for (int k = 0; k < elements; k++) {
						Failed[j-i][k] = covMat[j][k];
					}
				} else {
					i++;
				}
			}
		}
		return Failed;
	}
	
	//retorna vetor com o numero de casos de teste negativos que executecutam cada elemento
	private int[] getFailedN() {
		int[] failedTC = new int[elements];
		
		for (int i = 0; i < elements; i++) {
			for (int j = 0; j < covMatFailed.length; j++) {
				failedTC[i] += covMatFailed[j][i];
			}
		}
		
		return failedTC;
	}	

	//retorna vetor com o numero de casos de teste positivos que executecutam cada elemento
	private int[] getSuccessfulN() {
		int[] successfulTC = new int[elements];
		
		for (int i = 0; i < elements; i++) {
			for (int j = 0; j < covMatSuccessful.length; j++) {
				successfulTC[i] += covMatSuccessful[j][i];
			}
		}
		
		return successfulTC;
	}

	//retorna vetor de elementos
	private Element[] getElements() {
		Element [] E = new Element[this.elements];
		
		for (int i = 0; i < this.elements; i++) {
			E[i] = new Element(i, tests, sTests, fTests, qtdSuccessful[i], qtdFailed[i]);
		}
		
		return E;
		
	}
	
	//calcula suspeita do elemento e para a metrica de associacao M
	public double suspiciousness(int e, int M) {
		switch (M) {
		case 1:
			return this.Elements[e].M1();
		case 2:
			return this.Elements[e].M2();
		case 3:
			return this.Elements[e].M3();
		case 4:
			return this.Elements[e].M4();
		case 5:
			return this.Elements[e].M5();
		case 6:
			return this.Elements[e].M6();
		case 7:
			return this.Elements[e].M7();
		case 8:
			return this.Elements[e].M8();
		case 9:
			return this.Elements[e].M9();
		case 10:
			return this.Elements[e].M10();
		case 11:
			return this.Elements[e].M11();
		case 12:
			return this.Elements[e].M12();
		case 13:
			return this.Elements[e].M13();
		case 14:
			return this.Elements[e].M14();
		case 15:
			return this.Elements[e].M15();
		case 16:
			return this.Elements[e].M16();
		case 17:
			return this.Elements[e].M17();
		case 18:
			return this.Elements[e].M18();
		case 19:
			return this.Elements[e].M19();
		case 20:
			return this.Elements[e].M20();
		case 21:
			return this.Elements[e].M21();
		case 22:
			return this.Elements[e].M22();
		case 23:
			return this.Elements[e].M23();
		case 24:
			return this.Elements[e].M24();
		case 25:
			return this.Elements[e].M25();
		case 26:
			return this.Elements[e].M26();
		case 27:
			return this.Elements[e].M27();
		case 28:
			return this.Elements[e].M28();
		case 29:
			return this.Elements[e].M29();
		case 30:
			return this.Elements[e].M30();
		case 31:
			return this.Elements[e].M31();
		case 32:
			return this.Elements[e].M32();
        case 33:
            return this.Elements[e].M33();
        case 34:
            return this.Elements[e].M34();
		default:
			return 0;
		}
	}
	
	//calcula suspeita do elemento e para a metrica de associacao M
	public double spectraData(int e, int N) {
		switch (N) {
			case 1:
				return this.Elements[e].tests;
			case 2:
				return this.Elements[e].Ncf;
			case 3:
				return this.Elements[e].Nuf;
			case 4:
				return this.Elements[e].Ncs;
			case 5:
				return this.Elements[e].Nus;
			case 6:
				return this.Elements[e].Nc;
			case 7:
				return this.Elements[e].Nu;
			case 8:
				return this.Elements[e].Ns;
			case 9:
				return this.Elements[e].Nf;
			default:
				return 0;
		}
	}
}