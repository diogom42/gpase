package i4soft.inf.covData;

import java.util.ArrayList;

public class Element {	
	public int lineNumber;
	//private int [] children;//filhos se o elemento eh um bloco de controle
//	private double PE;//probabilidade de o elemento ser executado
//	private double PF;//probabilidade de o programa falhar
//	private double PnE;//probabilidade de o elemento nao ser executado
//	private double PnF;//probabilidade de o programa nao falhar
//	private double PEF;//probabilidade de o elemento ser executado e ocorrer uma falha
//	private double PnEF;//probabilidade de o elemento nao ser executado e falhar
//	private double PEnF;//probabilidade de o elemento ser executado e nao falhar
//	private double PnEnF;//probabilidade de o elemento nao ser executado e nao ocorrer uma falha
	public double tests;//numero total de casos de teste
	//public double sTests;//numero total de casos de teste positivo
	//public double fTests;//numero total de casos de tete negativos
	//public double qtdTC;//casos de teste que executam o elemento
	//public double qtdSuccessfulTC;//casos de teste que positivios que executam o elemento
	//public double qtdFailedTC;//casos de tete negativos que executam o elemento
    public double Ncf;//Number of failed test cases that cover the statement
    public double Nuf;//Number of failed test cases that do not cover the statement
    public double Ncs;//Number of successful test cases that cover the statement
    public double Nus;//Number of successful test cases that do not cover the statement
    public double Nc;//Total number of test cases that cover the statement
    public double Nu;//Total number of test cases that do not cover the statement
    public double Ns;//Total number of successful test cases
    public double Nf;//Total number of failed test cases
	
	public Element(int lineNumber, int tests, int Ns, int Nf, int Ncs, int Ncf) {
		this.lineNumber = lineNumber;
		this.Ncs = Ncs;
		this.Ncf = Ncf;
		this.Nc = Ncs + Ncf;
		this.Ns = Ns;
		this.Nf = Nf;
		this.tests = tests;
		this.Nu = tests - Ns;
        this.Nus = Ns - Ncs;
        this.Nuf = Nf - Ncf;
	}
	
	public double M1() {//Braun-Banquet
			return Ncf / Math.max(Ncf + Ncs, Ncf + Nuf);
	}

	public double M2() {//Dennis
	    return ((Ncf * Nus) - (Ncs * Nuf)) / ((Math.sqrt(tests * (Ncf + Ncs) * (Ncf + Nuf)) == 0) ? Double.MIN_VALUE : Math.sqrt(tests * (Ncf + Ncs) * (Ncf + Nuf)));
    }

	public double M3() {//Mountford
	    return Ncf / 0.5 * ((((Ncf * Ncs) + (Ncf * Nuf) + (Ncs * Nuf)) == 0) ? Double.MIN_VALUE : ((Ncf * Ncs) + (Ncf * Nuf) + (Ncs * Nuf)));
    }

    public double M4() {//Fossum
	    return tests * Math.pow(Ncf - 0.5, 2) / ((((Ncf + Ncs) * (Ncf + Nuf)) == 0) ? Double.MIN_VALUE : ((Ncf + Ncs) * (Ncf + Nuf)));
    }

	public double M5() {//Pearson
	    return tests * Math.pow((Ncf * Nus) - (Ncs * Nuf), 2) / ((Nc * Nu * Ns * Nf == 0) ? Double.MIN_VALUE : Nc * Nu * Ns * Nf);
    }

	public double M6() {//Gower
	    return (Ncs + Nus) / ((Math.sqrt(Nf * Nc * Nu * Ns) == 0) ? Double.MIN_VALUE : Math.sqrt(Nf * Nc * Nu * Ns));
    }

	public double M7() {//Michael
	    return 4 * ((Ncf * Nus) - (Ncs * Nuf)) /
                (((Math.pow(Ncf + Nus, 2) + Math.pow(Ncs + Nuf, 2)) == 0) ? Double.MIN_VALUE : (Math.pow(Ncf + Nus, 2) + Math.pow(Ncs + Nuf, 2)));
    }
	public double M8() {//Pierce
	    return ((Ncs * Nuf) + (Nuf * Ncs)) /
                ((((Ncf * Nuf) + (2 * (Nuf * Nus)) + (Ncs * Nus)) == 0) ? Double.MIN_VALUE : ((Ncf * Nuf) + (2 * (Nuf * Nus)) + (Ncs * Nus)));
    }

	public double M9() {//Baroni-Urbani & Buser
	    return (Math.sqrt(Ncf * Nus) + Ncf) /
                (((Math.sqrt(Ncf * Nus) + Ncf + Ncs + Nuf) == 0) ? Double.MIN_VALUE : (Math.sqrt(Ncf * Nus) + Ncf + Ncs + Nuf));
    }
	public double M10() {//Tarwid
	    return ((tests * Ncf) - (Nf * Nc)) / (((tests * Ncf) + (Nf * Nc) == 0) ? Double.MIN_VALUE : (tests * Ncf) + (Nf * Nc));
    }
	public double M11() {//Ample
	    return Math.abs((Ncf / (((Ncf + Nuf) == 0) ? Double.MIN_VALUE : (Ncf + Nuf))) -
                (Ncs / (((Ncs + Nus) == 0) ? Double.MIN_VALUE : (Ncs + Nus))));
    }
	public double M12() {//Phi (Geometric Mean)
	    return ((Ncf * Nus) - (Nuf * Ncs)) / ((Math.sqrt((Ncf + Ncs) * (Ncf + Nuf) * (Ncs + Nus) * (Nuf + Nus)) == 0) ?
                Double.MIN_VALUE : Math.sqrt((Ncf + Ncs) * (Ncf + Nuf) * (Ncs + Nus) * (Nuf + Nus)));
    }
	public double M13() {//Arithmetic Mean
	    return (2 * ((Ncf * Nus) - (Nuf * Ncs))) / ((((Ncf + Ncs) * (Nus + Nuf) + (Ncf + Nuf) * (Ncs + Nus)) == 0) ?
                Double.MIN_VALUE : ((Ncf + Ncs) * (Nus + Nuf) + (Ncf + Nuf) * (Ncs + Nus)));
    }
	public double M14() {//Cohen
	    return (2 * ((Ncf * Nus) - (Nuf * Ncs))) / ((((Ncf + Ncs) * (Nus + Ncs) + (Ncf + Nuf) * (Nuf + Nus)) == 0) ?
                Double.MIN_VALUE : ((Ncf + Ncs) * (Nus + Ncs) + (Ncf + Nuf) * (Nuf + Nus)));
    }
	public double M15() {//Fleiss
	    return (4 * ((Ncf * Nus) - (Nuf * Ncs)) - Math.pow(Nuf - Ncs, 2)) /
                ((((2 * Ncf + Nuf + Ncs) + (2 * Nus + Nuf + Ncs)) == 0) ? Double.MIN_VALUE : ((2 * Ncf + Nuf + Ncs) + (2 * Nus + Nuf + Ncs)));
    }
	public double M16() {//Zoltar
	    return Ncf / (((Ncf + Nuf + Ncs + (10000 * Nuf * Ncs / ((Ncf == 0) ? Double.MIN_VALUE : Ncf))) == 0) ? Double.MIN_VALUE : (Ncf + Nuf + Ncs + (10000 * Nuf * Ncs / ((Ncf == 0) ? Double.MIN_VALUE : Ncf))));
    }
	public double M17() {//Harmonic Mean
	    return (((Ncf * Nus) - (Nuf * Ncs)) * ((Ncf + Ncs) * (Nus + Nuf) + (Ncf + Nuf) * (Ncs + Nus))) /
				(((Ncf + Ncs) * (Nus + Nuf) * (Ncf + Nuf) * (Ncs + Nus) == 0) ? Double.MIN_VALUE : (Ncf + Ncs) * (Nus + Nuf) * (Ncf + Nuf) * (Ncs + Nus));
    }
	public double M18() {//Rogot2
	    return 0.5 * (Ncf / (((Ncf + Ncs) == 0) ? Double.MIN_VALUE : (Ncf + Ncs)) + Ncf / (((Ncf + Nuf) == 0) ? Double.MIN_VALUE : (Ncf + Nuf))
                + Nus / (((Nus + Ncs) == 0) ? Double.MIN_VALUE : (Nus + Ncs)) + Nus / (((Nus + Nuf) == 0) ? Double.MIN_VALUE : (Nus + Nuf)));
    }
	public double M19() {//Simple Matching
	    return (Ncf + Nus) / (((Ncf + Ncs + Nus + Nuf) == 0) ? Double.MIN_VALUE : (Ncf + Ncs + Nus + Nuf));
    }
	public double M20() {//Rogers & Tanimoto
	    return (Ncf + Nus) / (((Ncf + Nus + 2 * (Nus + Nuf)) == 0) ? Double.MIN_VALUE : (Ncf + Nus + 2 * (Nus + Nuf)));
    }
	public double M21() {//Hamming
	    return Ncf + Nus;
    }
	public double M22() {//Mamann
	    return (Ncf + Nus - Nuf - Ncs) / (((Ncf + Nuf + Ncs + Nus) == 0) ? Double.MIN_VALUE : (Ncf + Nuf + Ncs + Nus));
    }
	public double M23() {//Sokal
	    return 2 * (Ncf + Nus) / (((2 * (Ncf + Nus) + Nuf + Ncs) == 0) ? Double.MIN_VALUE : (2 * (Ncf + Nus) + Nuf + Ncs));
    }
	public double M24() {//Scott
	    return 4 * (Ncf * Nus - Nuf * Ncs) - Math.pow(Nuf - Ncs, 2) / ((((2 * Ncf + Nuf + Ncs) * (2 * Nus + Nuf + Ncs)) == 0) ? Double.MIN_VALUE : ((2 * Ncf + Nuf + Ncs) * (2 * Nus + Nuf + Ncs)));
    }
	public double M25() {//Rogot1
	    return 0.5 * ((Ncf / (((2 * Ncf + Nuf + Ncs) == 0) ? Double.MIN_VALUE : (2 * Ncf + Nuf + Ncs))) + (Nus / (((2 * Nus + Nuf + Ncs) == 0) ? Double.MIN_VALUE : (2 * Nus + Nuf + Ncs))));
    }
	public double M26() {//Kulczynski
	    return Ncf / (((Nuf + Ncs) == 0) ? Double.MIN_VALUE : (Nuf + Ncs));
    }
	public double M27() {//Anderberg
	    return Ncf / (((Ncf + 2 * (Nuf + Ncs)) == 0) ? Double.MIN_VALUE : (Ncf + 2 * (Nuf + Ncs)));
    }
	public double M28() {//Dice
	    return 2 * Ncf/ (((Ncf + Nuf + Ncs) == 0) ? Double.MIN_VALUE : (Ncf + Nuf + Ncs));
    }
	public double M29() {//Goodman
	    return 2 * Ncf - Nuf - Ncs/ (((2 * Ncf + Nuf + Ncs) == 0) ? Double.MIN_VALUE : (2 * Ncf + Nuf + Ncs));
    }
	public double M30() {//Jaccard
	    return Ncf / (((Ncf + Nuf + Ncs) == 0) ? Double.MIN_VALUE : (Ncf + Nuf + Ncs));
    }
	public double M31() {//Sorense-Dice
		return 2 * Ncf / (((2 * Ncf + Nuf + Ncs) == 0) ? Double.MIN_VALUE : (2 * Ncf + Nuf + Ncs));
	}
	
	public double M32() {//Tarantula
		return (Ncf/((Nf == 0) ? Double.MIN_VALUE : Nf)) / ((((Ncs/Ns) + (Ncf/Nf)) == 0) ? Double.MIN_VALUE : ((Ncs/Ns) + (Ncf/Nf)));
	}
	
	public double M33() {//Ochiai
		return Ncf/Math.sqrt(Nf * (((Ncf == 0) ? Double.MIN_VALUE : Ncf) + ((Ncs == 0) ? Double.MIN_VALUE : Ncs)));
	}

	public double M34() {//Ochiai2
	    return Ncf * Nus / (((Math.sqrt((Ncf + Ncs) * (Nus + Nuf) * (Ncf + Nuf) * (Ncs + Nus))) == 0) ? Double.MIN_VALUE : (Math.sqrt((Ncf + Ncs) * (Nus + Nuf) * (Ncf + Nuf) * (Ncs + Nus))));
    }
}
