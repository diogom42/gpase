package i4soft.inf.covData;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LoadFiles {

	private static int iVer[];
	
	private static int nFailures[];
	
	private static int nLines[];
	
	private static int nTC[];
	
	private static int[] programsSet;
	
	private  double[][][] Metrics;
	
	private  double[][][] Spectra;

	private  ArrayList<ArrayList<Integer>> faults;
	
	private  int tLines;
	
	private int nVer;
	
	//construtor para populacao com todos os programas
	
	public LoadFiles() {
		programsSet = null;
		nVer = 131;
		
		iVer = readCSV("inputs/iVer.csv");
		nFailures = readCSV("inputs/nFailures.csv");
		nLines = readCSV("inputs/nLines.csv");
		nTC = readCSV("inputs/nTC.csv");
		
		Metrics = calcMetrics();
		Spectra = calcSpectraData();
		faults = readFaults("inputs/faults.csv");
		tLines = sumLines();
	}
	
	public LoadFiles(String ProgramsSetFile) {
		programsSet = readTestSet(ProgramsSetFile);
		nVer = programsSet.length;

		iVer = readCSV("inputs/iVer.csv");
		nFailures = readCSV("inputs/nFailures.csv");
		nLines = readCSV("inputs/nLines.csv");
		nTC = readCSV("inputs/nTC.csv");

		Metrics = calcMetrics();
		Spectra = calcSpectraData();
		faults = readFaults("inputs/faults.csv");
		tLines = sumLines();
	}
	
	private int[] readTestSet(String FileName) {
		int[] csvMatrix = new int[1];
		
        BufferedReader br = null;
        String line = "";

        try {

            br = new BufferedReader(new FileReader(FileName));
            
            while ((line = br.readLine()) != null) {
            	
            	String[] Line = line.split(",");
            	csvMatrix = new int[Line.length];
            	
                for (int j = 0; j < Line.length; j++) {
                	csvMatrix[j] = Integer.parseInt(Line[j]);
				}

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return csvMatrix;
	}
	
	private int[] readCSV(String FileName) {
		int[] csvMatrix = new int[1];
		
        BufferedReader br = null;
        String line = "";

        try {

            br = new BufferedReader(new FileReader(FileName));
            
            while ((line = br.readLine()) != null) {
            	
            	String[] Line = line.split(",");
            	csvMatrix = new int[nVer];
            	
            	int i = 0;
                for (int j = 0; j < Line.length; j++) {
                	if (isInProgramsSet(j)) {
                		csvMatrix[i++] = Integer.parseInt(Line[j]);
                	}
				}

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return csvMatrix;
	}
	
	private double[][][] calcMetrics() {
		double [][][] Metrics = new double[nVer][34][4000];
	  
		CovMatrix cov;
		
		int i = 0;
		for (int j = 0; j < 131; j++) {
			if (isInProgramsSet(j)) {
				if (j <= 4) {
					cov = new CovMatrix("inputs/prog1/covMatV" + iVer[i] + ".csv", "inputs/prog1/failedV" + iVer[i] + ".csv", "inputs/prog1/controlBlocksv" + iVer[i] + ".csv", nTC[i], nLines[i]);
				} else {
					if (j <= 14) {
						cov = new CovMatrix("inputs/prog2/covMatV" + iVer[i] + ".csv", "inputs/prog2/failedV" + iVer[i] + ".csv", "inputs/prog2/controlBlocksv" + iVer[i] + ".csv", nTC[i], nLines[i]);
					} else {
						if (j <= 44) {
							cov = new CovMatrix("inputs/prog3/covMatV" + iVer[i] + ".csv", "inputs/prog3/failedV" + iVer[i] + ".csv", "inputs/prog3/controlBlocksv" + iVer[i] + ".csv", nTC[i], nLines[i]);
						} else {
							if (j <= 53) {
								cov = new CovMatrix("inputs/prog4/covMatV" + iVer[i] + ".csv", "inputs/prog4/failedV" + iVer[i] + ".csv", "inputs/prog4/controlBlocksv" + iVer[i] + ".csv", nTC[i], nLines[i]);
							} else {
								if (j <= 62) {
									cov = new CovMatrix("inputs/prog5/covMatV" + iVer[i] + ".csv", "inputs/prog5/failedV" + iVer[i] + ".csv", "inputs/prog5/controlBlocksv" + iVer[i] + ".csv", nTC[i], nLines[i]);
								} else {
									if (j <=98) {
										cov = new CovMatrix("inputs/prog6/covMatV" + iVer[i] + ".csv", "inputs/prog6/failedV" + iVer[i] + ".csv", "inputs/prog6/controlBlocksv" + iVer[i] + ".csv", nTC[i], nLines[i]);
									} else {
										if (j <= 117) {
											cov = new CovMatrix("inputs/prog7/covMatV" + iVer[i] + ".csv", "inputs/prog7/failedV" + iVer[i] + ".csv", nTC[i], nLines[i]);
										} else {
											cov = new CovMatrix("inputs/prog8/covMatV" + iVer[i] + ".csv", "inputs/prog8/failedV" + iVer[i] + ".csv", nTC[i], nLines[i]);
										}
									}
								}
							}
						}
					}
				}
				for (int j2 = 0; j2 < 34; j2++) {
					for (int j3 = 0; j3 < nLines[i]; j3++) {
						Metrics[i][j2][j3] = cov.suspiciousness(j3, j2+1);
					}
				}
				i++;
			}
		}
	  
		return Metrics;
	}
	
	private double[][][] calcSpectraData() {
		double [][][] Spectra = new double[nVer][9][4000];
	  
		CovMatrix cov;
		int i = 0;
		for (int j = 0; j < 131; j++) {
			//int nLinhas;
			if (isInProgramsSet(j)) {
				if (j <= 4) {
					cov = new CovMatrix("inputs/prog1/covMatV" + iVer[i] + ".csv", "inputs/prog1/failedV" + iVer[i] + ".csv", "inputs/prog1/controlBlocksv" + iVer[i] + ".csv", nTC[i], nLines[i]);
				} else {
					if (j <= 14) {
						cov = new CovMatrix("inputs/prog2/covMatV" + iVer[i] + ".csv", "inputs/prog2/failedV" + iVer[i] + ".csv", "inputs/prog2/controlBlocksv" + iVer[i] + ".csv", nTC[i], nLines[i]);
					} else {
						if (j <= 44) {
							cov = new CovMatrix("inputs/prog3/covMatV" + iVer[i] + ".csv", "inputs/prog3/failedV" + iVer[i] + ".csv", "inputs/prog3/controlBlocksv" + iVer[i] + ".csv", nTC[i], nLines[i]);
						} else {
							if (j <= 53) {
								cov = new CovMatrix("inputs/prog4/covMatV" + iVer[i] + ".csv", "inputs/prog4/failedV" + iVer[i] + ".csv", "inputs/prog4/controlBlocksv" + iVer[i] + ".csv", nTC[i], nLines[i]);
							} else {
								if (j <= 62) {
									cov = new CovMatrix("inputs/prog5/covMatV" + iVer[i] + ".csv", "inputs/prog5/failedV" + iVer[i] + ".csv", "inputs/prog5/controlBlocksv" + iVer[i] + ".csv", nTC[i], nLines[i]);
								} else {
									if (j <=98) {
										cov = new CovMatrix("inputs/prog6/covMatV" + iVer[i] + ".csv", "inputs/prog6/failedV" + iVer[i] + ".csv", "inputs/prog6/controlBlocksv" + iVer[i] + ".csv", nTC[i], nLines[i]);
									} else {
										if (j <= 117) {
											cov = new CovMatrix("inputs/prog7/covMatV" + iVer[i] + ".csv", "inputs/prog7/failedV" + iVer[i] + ".csv", nTC[i], nLines[i]);
										} else {
											cov = new CovMatrix("inputs/prog8/covMatV" + iVer[i] + ".csv", "inputs/prog8/failedV" + iVer[i] + ".csv", nTC[i], nLines[i]);
										}
									}
								}
							}
						}
					}
				}
				for (int j2 = 0; j2 < 9; j2++) {
					for (int j3 = 0; j3 < nLines[i]; j3++) {
						Spectra[i][j2][j3] = cov.spectraData(j3, j2+1);
					}
				}
				i++;
			}
		}
		return Spectra;
	}
  
	private ArrayList<ArrayList<Integer>> readFaults(String FileName) {
		ArrayList<ArrayList<Integer>> faults = new ArrayList<ArrayList<Integer>>();
		
        BufferedReader br = null;
        String line = "";

        try {

            br = new BufferedReader(new FileReader(FileName));
            
            int i = 0;//contador de linhas do arquivo
            int j = 0;//contador de versoes
            while ((line = br.readLine()) != null) {
            	        		
        		if (isInProgramsSet(i)) {
        			faults.add(new ArrayList<Integer>());
                	
            		if (!line.equals("")) {
            			String[] Line = line.split(",");
                    	for (int j1 = 0; j1 < Line.length; j1++) {
                        	faults.get(j).add(Integer.parseInt(Line[j1]));
        				}
                    }
            		j++;
        		}
            	i++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return faults;
	}
	
	private int sumLines() {
		int lines = 0;
		int i = 0;
		for (int j = 0; j < nLines.length; j++) {
			if (isInProgramsSet(j)) {
				lines += nLines[i];
				i++;
			}
		}
		return lines;
	}

	public int[] getIVer() {
		return iVer;
	}
	
	public int[] getNLines() {
		return nLines;
	}
	
	public int[] getNTC() {
		return nTC;
	}
	
	public double[][][] getMetrics() {
		return Metrics;
	}
	
	public double[][][] getSpectra() {
		return Spectra;
	}
	
	public ArrayList<ArrayList<Integer>> getfaults() {
		return faults;
	}
	
	public int getTLines() {
		return tLines;
	}
	
	public int getNVer() {
		return nVer;
	}

    public static boolean isInProgramsSet(int ver) {
        if (programsSet == null) {
            return true;
        }
        for (int i = 0; i < programsSet.length; i++) {
            if (ver < programsSet[i]) {
                return false;
            } else {
                if (ver == programsSet[i]) {
                    return true;
                }
            }
        }

        return false;
    }
	
	public int getNFaults() {
		int sum = 0;
		for (int i = 0; i < nFailures.length; i++) {
			sum = nFailures[i];
		}
		return sum;
	}
}
