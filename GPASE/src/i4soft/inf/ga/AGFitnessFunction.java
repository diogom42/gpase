package i4soft.inf.ga;

import org.jgap.FitnessFunction;
import org.jgap.IChromosome;
import org.jgap.impl.FixedBinaryGene;

public class AGFitnessFunction extends FitnessFunction {
	
	private static final long serialVersionUID = 1L;

	public double evaluate(IChromosome a_subject) {
		  return computeRawFitness(a_subject);
	}
	  
	public static double computeRawFitness(IChromosome ind) {
		double error = 0;
	    
		for (int i = 0; i < MetricsGA.nVer; i++) {
			double result[][] = new double [MetricsGA.nLines[i]][2];
				
			for (int j = 0; j < MetricsGA.nLines[i]; j++) {
				result[j][0] = j;
				result[j][1] = suspiciousness(ind, i, j);
			}
			// O erro eh a prporcao de linhas necessarias para alcancar o defeito
			// -------------------------------------------------------------------
			error += getError(result, i);
		}
		return error;///nVer;//tLines;
	}
	
	
	public static double suspiciousness(IChromosome a_potentialSolution, int ver, int element) {
		double suspiciousness = 0;
		for (int i = 0; i < 34; i++) {
			suspiciousness = suspiciousness + (MetricsGA.Metrics[ver][i][element] * ((Double)a_potentialSolution.getGene(i).getAllele()).doubleValue());//representacao real
		}

//		System.out.println("T: " + suspiciousness);
		return suspiciousness;
	}
	  
	  
	public static double getWeightAtGene(IChromosome a_potentialSolution, int a_position) {
		double weight = 0;
		for(int i = 0; i < 7; i++) {
			if(((FixedBinaryGene)a_potentialSolution.getGene(a_position)).getBit(i)) {//representacao 34 genes 7 bits
				weight = weight + Math.pow(2,6-i);
			}
		}
		return weight/127;//divide por 2^7 - 1
	}

	private static double getError(double result[][], int ver) {
		double error = 0;
			
		//ordena o vetor com os resultados do individuo
		java.util.Arrays.sort(result, new java.util.Comparator<double[]>() {
			public int compare(double[] a, double[] b) {
				return Double.compare(b[1], a[1]);
			}
		});
						
		int nFaults = MetricsGA.faults.get(ver).size();//qtd de falhas na versao
		for (int i = 0; i < MetricsGA.nLines[ver]; i++) {
			error++;
			if (MetricsGA.faults.get(ver).contains((int)result[i][0])) {
				nFaults--;//encontrou uma falha
			}
			if (i+1 < MetricsGA.nLines[ver] && nFaults == 0 && result[i][1] != result[i+1][1])
				break;
			}
			
			return 100*error/ MetricsGA.nLines[ver];//o valor deve ser poporcional a quantidade de linhas da versao
		}
	  
	}