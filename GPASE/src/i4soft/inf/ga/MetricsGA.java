package i4soft.inf.ga;

import java.util.ArrayList;
import i4soft.inf.covData.*;
import org.jgap.*;
import org.jgap.impl.*;

public class MetricsGA {

  /**
   * The total number of times we'll let the population evolve.
   */
	private static int GEN_QUANTITY;
	private static int POP_SIZE;
	private static int TRAINING_SET;
	private static int PROGRAM;

	private static LoadFiles data;// = new LoadFiles("inputs/testSet" + TEST_SET + ".csv");
	protected static int nVer;// = data.getNVer();
	protected static int nLines[];// = data.getNLines();
	protected static double[][][] Metrics;// = data.getMetrics();
	protected static ArrayList<ArrayList<Integer>> faults;// = data.getfaults();

	protected static int iVer[];// = data.getIVer();
	protected static int nTC[];// = data.getNTC();
	protected static int tLines;// = data.getTLines();
	protected static double[][][] Spectra;// = data.getSpectra();


	public static ArrayList<ArrayList<Double>> exec(int n, int pop, int gen, int prog, int set) throws Exception {
		POP_SIZE = pop;
		GEN_QUANTITY = gen;
		TRAINING_SET = set;
		PROGRAM = prog;

		System.out.println("Exec: " + n +  ", Pop: " + POP_SIZE + ", Gen: " + GEN_QUANTITY + ", Program: " + PROGRAM +
				", Test Set: " + TRAINING_SET);

		if (PROGRAM < 1 || PROGRAM >7) {
			data = new LoadFiles("inputs/trainningSet" + TRAINING_SET + ".csv");
		} else {
			data = new LoadFiles("inputs/prog" + PROGRAM + "/trainningSet" + TRAINING_SET + ".csv");
		}
		nVer = data.getNVer();
		nLines = data.getNLines();
		iVer = data.getIVer();
		nTC = data.getNTC();
		tLines = data.getTLines();
		Metrics = data.getMetrics();
		Spectra = data.getSpectra();
		faults = data.getfaults();
	  
	  	// Start with a DefaultConfiguration, which comes setup with the
		// most common settings.
		// -------------------------------------------------------------
		Configuration.reset();
		Configuration conf = new DefaultConfiguration();
		// Care that the fittest individual of the current population is
		// always taken to the next generation.
		// Consider: With that, the pop. size may exceed its original
		// size by one sometimes!
		// -------------------------------------------------------------
		
		conf.setPreservFittestIndividual(true);
		conf.setKeepPopulationSizeConstant(true);
		// Set the fitness function we want to use, which is our
		// MinimizingMakeChangeFitnessFunction. We construct it with
		// the target amount of change passed in to this method.
		// ---------------------------------------------------------
		FitnessFunction myFunc = new AGFitnessFunction();
		conf.setFitnessFunction(myFunc);
		conf.resetProperty(Configuration.PROPERTY_FITEVAL_INST);
		conf.setFitnessEvaluator(new DeltaFitnessEvaluator());//minimizar fitness
		
		conf.setPopulationSize(POP_SIZE);
		conf.setRandomGenerator(new GaussianRandomGenerator());
		
		GeneticOperator crossover = new CrossoverOperator(conf, 0.9, true);
		conf.addGeneticOperator(crossover);
		
		Gene[] weights = new Gene[34];
		for (int i = 0; i < 34; i++) {
			weights[i] = new DoubleGene(conf, 0, 1);
		}
		IChromosome weightsChromosome = new Chromosome(conf, weights);
	    conf.setSampleChromosome(weightsChromosome);
		
		// Finally, we need to tell the Configuration object how many
		// Chromosomes we want in our population. The more Chromosomes,
		// the larger number of potential solutions (which is good for
		// finding the answer), but the longer it will take to evolve
		// the population (which could be seen as bad).
		// ------------------------------------------------------------
		
		// Create random initial population of Chromosomes.
		Genotype population;
		// Now we initialize the population randomly
		population = Genotype.randomInitialGenotype(conf);

		System.out.println("genetic operators: " + conf.getGeneticOperators());
		System.out.println("minimum pop size percent: " + conf.getMinimumPopSizePercent());
		System.out.println("natural selector: " + conf.getNaturalSelectors(true).size());
		System.out.println("natural selector: " + conf.getNaturalSelectors(false).size());
		
		// Evolve the population. Since we don't know what the best answer
		// is going to be, we just evolve the max number of times.
		// ---------------------------------------------------------------
		long startTime = System.currentTimeMillis();
		IChromosome bestSolutionSoFar;
		double bestfitness = Double.MAX_VALUE;
		for (int i = 0; i < GEN_QUANTITY; i++) {
			if (!uniqueChromosomes(population.getPopulation())) {
				throw new RuntimeException("Invalid state in generation "+i);
	        }
//			System.out.println("Evolving gen " + i);
			
			bestSolutionSoFar = population.getFittestChromosome();
			if (bestfitness > bestSolutionSoFar.getFitnessValue()) {
				bestfitness = bestSolutionSoFar.getFitnessValue();
//				System.out.println("Found better fitness:" + bestfitness + "(" + bestfitness/nVer + " %)");
//				System.out.println("It contains the following: ");
//				System.out.print("M1: " + AGFitnessFunction.getWeightAtGene(bestSolutionSoFar, 0));
//				for (int j = 1; j < 21; j++) {
//					System.out.print(", M" + (j + 1) + ": " + AGFitnessFunction.getWeightAtGene(bestSolutionSoFar, j));
//				}
//				System.out.print(", M22: " + AGFitnessFunction.getWeightAtGene(bestSolutionSoFar, 21) + "\n");
//				System.out.print("M1: " + ((Double)bestSolutionSoFar.getGene(0).getAllele()).doubleValue());
//				for (int j = 1; j < 21; j++) {
//					System.out.print(", M" + (j + 1) + ": " + ((Double)bestSolutionSoFar.getGene(j).getAllele()).doubleValue());
//				}
//				System.out.print(", M22: " + ((Double)bestSolutionSoFar.getGene(21).getAllele()).doubleValue() + "\n");
			}
			population.evolve(1);
			
//			for (int j = 0; j < population.getPopulation().size(); j++) {
//				System.out.println(population.getPopulation().getChromosome(j));
//				System.out.println("Sol. " + j +" :" + population.getPopulation().getChromosome(j).getFitnessValue());
//			}
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Total evolution time: " + ( endTime - startTime) + " ms");
		
		// Display the best solution we found.
		// -----------------------------------
		bestSolutionSoFar = population.getFittestChromosome();
		
		System.out.println("Fitnes training: " + bestSolutionSoFar.getFitnessValue() + "(" + bestSolutionSoFar.getFitnessValue()/nVer + " %)");
		bestSolutionSoFar.setFitnessValueDirectly(-1);

		ArrayList<Double> Weight = new ArrayList();
		for (int j = 0; j < 34; j++) {
			Weight.add(((Double)bestSolutionSoFar.getGene(j).getAllele()).doubleValue());
		}

		System.out.println("It contains the following: " + Weight.toString());

		if (PROGRAM < 1 || PROGRAM >7) {
			data = new LoadFiles("inputs/testSet" + TRAINING_SET + ".csv");
		} else {
			data = new LoadFiles("inputs/prog" + PROGRAM + "/testSet" + TRAINING_SET + ".csv");
		}
		nVer = data.getNVer();
		nLines = data.getNLines();
		iVer = data.getIVer();
		nTC = data.getNTC();
		tLines = data.getTLines();
		Metrics = data.getMetrics();
		Spectra = data.getSpectra();
		faults = data.getfaults();

		ArrayList<ArrayList<Double>> results = new ArrayList<ArrayList<Double>>();

		results.add(new ArrayList());
		results.get(0).add(bestSolutionSoFar.getFitnessValue()/nVer);
		System.out.println("Fitnes test: " + results.get(0).get(0));
		results.add(testGAMetricPer(Weight));
		results.add(testGAMetricLin(Weight));
		results.add(testGPMetric(Weight));

		return results;

  	}

	private static ArrayList testGPMetric(ArrayList<Double> Weight) {
		ArrayList results = new ArrayList();
		double acca1 = 0;
		double acca3 = 0;
		double acca5 = 0;
		double acca10 = 0;
		double wefa1 = 0;
		double wefa3 = 0;
		double wefa5 = 0;
		double wefa10 = 0;
		Object[] noargs = new Object[0];
		// Evaluate function for input numbers 0 to 20.
		// --------------------------------------------

		for (int i = 0; i < nVer; i++) {
			double result[][] = new double [nLines[i]][2];

			for (int j = 0; j < nLines[i]; j++) {
				result[j][0] = j;
				result[j][1] = 0;
				for (int j1 = 0; j1 < Weight.size(); j1++) {
					result[j][1] += Weight.get(j1) * Metrics[i][j1][j];
				}
			}
			acca1 += accan(1, result, i);//gets the number of faults located in the n first ranks
			acca3 += accan(3, result, i);
			acca5 += accan(5, result, i);
			acca10 += accan(10, result, i);
			wefa1 += wefan(1, result, i);
			wefa3 += wefan(3, result, i);
			wefa5 += wefan(5, result, i);
			wefa10 += wefan(10, result, i);
		}
		System.out.println("acc@1: " + acca1);
		System.out.println("acc@3: " + acca3);
		System.out.println("acc@5: " + acca5);
		System.out.println("acc@10: " + acca10);
		System.out.println("wef@1: " + wefa1);
		System.out.println("wef@3: " + wefa3);
		System.out.println("wef@5: " + wefa5);
		System.out.println("wef@10: " + wefa10);

		results.add(acca1);
		results.add(acca3);
		results.add(acca5);
		results.add(acca10);
		results.add(wefa1);
		results.add(wefa3);
		results.add(wefa5);
		results.add(wefa10);

		return results;
	}

	private static int accan(int n, double result[][], int ver) {
		int qtd = 0;//qtd of faulty elements in the top n rank positions

		//ordena o vetor com os resultados do individuo
		java.util.Arrays.sort(result, new java.util.Comparator<double[]>() {
			public int compare(double[] a, double[] b) {
				return Double.compare(b[1], a[1]);
			}
		});

		for (int i = 0; i < n && i < nLines[ver]; i++) {
			if (faults.get(ver).contains((int)result[i][0])) {
				qtd++;
			}
		}

		return qtd;
	}

	private static int wefan(int n, double result[][], int ver) {
		int qtd = 0;//quantity of non faulty elements in the rank befor reach a fault or the nth element

		//ordena o vetor com os resultados do individuo
		java.util.Arrays.sort(result, new java.util.Comparator<double[]>() {
			public int compare(double[] a, double[] b) {
				return Double.compare(b[1], a[1]);
			}
		});

		for (int i = 0; i < n && i < nLines[ver]; i++) {
			if (faults.get(ver).contains((int)result[i][0])) {
				break;//found a fault
			} else {
				qtd++;//did not found a fault
			}
		}

		return qtd;
	}








	/**
	 * proportion of located faults per instpected code ratio
	 * from 10% to 100% of code inspected
	 * shows percentage of located faults
	 * @param Weight the weights of a individual
	 */
	private static ArrayList testGAMetricPer(ArrayList<Double> Weight) {
		ArrayList results = new ArrayList();

		int nFaults = 0;
		for(int i = 0; i < faults.size(); i++) {
			nFaults += faults.get(i).size();
		}
		Object[] noargs = new Object[0];

		double[] error = new double[10];
		for (int k = 0; k < 10; k++) {
			error[k] = 0;
			for (int i = 0; i < nVer; i++) {
				//			if (isInTraningSet(i)) {
				double result[][] = new double [nLines[i]][2];

				for (int j = 0; j < nLines[i]; j++) {
					result[j][0] = j;
					result[j][1] = 0;
					for (int j1 = 0; j1 < Weight.size(); j1++) {
						result[j][1] += Weight.get(j1) * Metrics[i][j1][j];
					}
				}
				//error += getError(result, i);
				//System.out.println("Erro v" + i + ": " + getError(result, i));
				error[k] += qtdFaults(result, i, 0.1 * (k+1));
				//System.out.println( qtdFaults(result, i, 0.1));//getError(result, i));
				//			}
			}
		}
		//System.out.println("M" + M + ": Media = " + 100 * error/nVer + "%");
		String result = "";
		for (int i = 0; i < error.length; i++) {
			results.add(100 * error[i]/nFaults);
			result += String.valueOf(results.get(i)) + ";";
		}
		System.out.println("GA" + PROGRAM + "/" + TRAINING_SET + "(" + nFaults + ");" + result);

		return results;
	}

	/**
	 * proportion of located faults per number of inspected lines
	 * from 5 lines to 50 lines
	 * percentage of located faults with x lines of code investigated
	 * @param Weight the weights of a individual
	 */
	private static ArrayList testGAMetricLin(ArrayList<Double> Weight) {
		ArrayList results = new ArrayList();

		int nFaults = 0;
		for(int i = 0; i < faults.size(); i++) {
			nFaults += faults.get(i).size();
		}
		Object[] noargs = new Object[0];

		double[] error = new double[10];
		for (int k = 0; k < 10; k++) {
			error[k] = 0;
			for (int i = 0; i < nVer; i++) {
				//			if (isInTraningSet(i)) {
				double result[][] = new double [nLines[i]][2];

				for (int j = 0; j < nLines[i]; j++) {
					result[j][0] = j;
					result[j][1] = 0;
					for (int j1 = 0; j1 < Weight.size(); j1++) {
						result[j][1] += Weight.get(j1) * Metrics[i][j1][j];
					}
				}
				//error += getError(result, i);
				//System.out.println("Erro v" + i + ": " + getError(result, i));
				error[k] += qtdFaultsLines(result, i, (k+1));
				//System.out.println( qtdFaults(result, i, 0.1));//getError(result, i));
				//			}
			}
		}
		//System.out.println("M" + M + ": Media = " + 100 * error/nVer + "%");
		String result = "";
		for (int i = 0; i < error.length; i++) {
			results.add(100 * error[i]/nFaults);
			result += String.valueOf(results.get(i)) + ";";
		}
		System.out.println("GA" + PROGRAM + "/" + TRAINING_SET + "(" + nFaults + ");" + result);

		return results;
	}

	private static int qtdFaultsLines(double result[][], int ver, double lines) {
		int qtd = 0;

		//ordena o vetor com os resultados do individuo
		java.util.Arrays.sort(result, new java.util.Comparator<double[]>() {
			public int compare(double[] a, double[] b) {
				return Double.compare(b[1], a[1]);
			}
		});

		for (int i = 0; i < lines; i++) {
			if (faults.get(ver).contains((int)result[i][0])) {
				qtd++;
			}
		}

		return qtd;
	}

	private static int qtdFaults(double result[][], int ver, double proportion) {
		int qtd = 0;

		//ordena o vetor com os resultados do individuo
		java.util.Arrays.sort(result, new java.util.Comparator<double[]>() {
			public int compare(double[] a, double[] b) {
				return Double.compare(b[1], a[1]);
			}
		});

		for (int i = 0; i < proportion*nLines[ver]; i++) {
			if (faults.get(ver).contains((int)result[i][0])) {
				qtd++;
			}
		}

		return qtd;
	}



  /**
   * @param a_pop the population to verify
   * @return true if all chromosomes in the populationa are unique
   *
   * @author Klaus Meffert
   * @since 3.3.1
   */
  public static boolean uniqueChromosomes(Population a_pop) {
    // Check that all chromosomes are unique
    for(int i=0;i<a_pop.size()-1;i++) {
      IChromosome c = a_pop.getChromosome(i);
      for(int j=i+1;j<a_pop.size();j++) {
        IChromosome c2 =a_pop.getChromosome(j);
        if (c == c2) {
          return false;
        }
      }
    }
    return true;
  }
  
}
