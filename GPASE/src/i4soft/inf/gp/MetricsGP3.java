package i4soft.inf.gp;

import i4soft.inf.covData.LoadFiles;
import org.jgap.InvalidConfigurationException;
import org.jgap.gp.CommandGene;
import org.jgap.gp.GPFitnessFunction;
import org.jgap.gp.GPProblem;
import org.jgap.gp.IGPProgram;
import org.jgap.gp.function.*;
import org.jgap.gp.impl.DeltaGPFitnessEvaluator;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.gp.impl.GPGenotype;
import org.jgap.gp.terminal.Terminal;
import org.jgap.gp.terminal.Variable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;


public class MetricsGP3 extends GPProblem {

	private static final int POP_MAX_DEPTH = 10;
	private static final int POP_MIN_DEPTH = 2;
	private static final int NODE_MAX_NUMBER = 100;
	private static final int CROSSOVER_MAX_DEPTH = 20;

	private static int GEN_QUANTITY;
	private static int POP_SIZE;
	private static int TRAINING_SET;
	private static int PROGRAM;

	public static Variable N;
	public static Variable Ncf;
	public static Variable Nuf;
	public static Variable Ncs;
	public static Variable Nus;
	public static Variable Nc;
	public static Variable Nu;
	public static Variable Ns;
	public static Variable Nf;

	private static LoadFiles data;// = new LoadFiles("inputs/testSet" + TEST_SET + ".csv");
	private static int nVer;// = data.getNVer();
	private static int nLines[];// = data.getNLines();
	//private static double[][][] Metrics;// = data.getMetrics();
	private static ArrayList<ArrayList<Integer>> faults;// = data.getfaults();
	private static double[] funcError;// = new double[POP_SIZE];
	private static double[][][] Spectra;// = data.getSpectra();

	private static int iVer[];// = data.getIVer();
	private static int nTC[];// = data.getNTC();
	private static int tLines;// = data.getTLines();


	public MetricsGP3(GPConfiguration a_conf) throws InvalidConfigurationException {
		super(a_conf);
	}

	public GPGenotype create() throws InvalidConfigurationException {
		GPConfiguration conf = getGPConfiguration();

		Class[] types = {CommandGene.FloatClass};

		Class[][] argTypes = {{}};

		CommandGene[][] nodeSets = { {
	        // We use a variable that can be set in the fitness function.
	        // ----------------------------------------------------------
			N = Variable.create(conf, "N", CommandGene.FloatClass),
			Ncf = Variable.create(conf, "Ncf", CommandGene.FloatClass),
			Nuf = Variable.create(conf, "Nuf", CommandGene.FloatClass),
			Ncs = Variable.create(conf, "Ncs", CommandGene.FloatClass),
			Nus = Variable.create(conf, "Nus", CommandGene.FloatClass),
			Nc = Variable.create(conf, "Nc", CommandGene.FloatClass),
			Nu = Variable.create(conf, "Nu", CommandGene.FloatClass),
			Ns = Variable.create(conf, "Ns", CommandGene.FloatClass),
			Nf = Variable.create(conf, "Nf", CommandGene.FloatClass),
	        new Add(conf, CommandGene.FloatClass),
//	        new Add3(conf, CommandGene.FloatClass),
	        new Multiply(conf, CommandGene.FloatClass),
//	        new Multiply3(conf, CommandGene.FloatClass),
	        new Divide(conf, CommandGene.FloatClass),
//	        new Sine(conf, CommandGene.FloatClass),
//	        new Exp(conf, CommandGene.FloatClass),
	        new Subtract(conf, CommandGene.FloatClass),
//	        new Pow(conf, CommandGene.FloatClass),
	        new Max(conf, CommandGene.FloatClass),
	        new Log(conf, CommandGene.FloatClass),
	        new Sqrt(conf, CommandGene.FloatClass),
	        new Terminal(conf, CommandGene.FloatClass, 0.0d, 1.0d, false),
		}, {} };

	    return GPGenotype.randomInitialGenotype(conf, types, argTypes, nodeSets, NODE_MAX_NUMBER , true);
	}


	public static ArrayList<ArrayList<Double>> exec(int n, int pop, int gen, int prog, int set) throws Exception {
		POP_SIZE = pop;
		GEN_QUANTITY = gen;
		TRAINING_SET = set;
		PROGRAM = prog;

		System.out.println("Exec: " + n +  ", Pop: " + POP_SIZE + ", Gen: " + GEN_QUANTITY + ", Program: " + PROGRAM +
				", Test Set: " + TRAINING_SET);

		if (PROGRAM < 1 || PROGRAM >7) {
			data = new LoadFiles("inputs/trainningSet" + TRAINING_SET + ".csv");
		} else {
			data = new LoadFiles("inputs/prog" + PROGRAM + "/trainningSet" + TRAINING_SET + ".csv");
		}
		nVer = data.getNVer();
		nLines = data.getNLines();
		iVer = data.getIVer();
		nTC = data.getNTC();
		tLines = data.getTLines();
		//Metrics = data.getMetrics();
		Spectra = data.getSpectra();
		faults = data.getfaults();
		funcError = new double[POP_SIZE];

		// Setup the algorithm's parameters.
	    // ---------------------------------
		GPConfiguration.reset();
	    GPConfiguration config = new GPConfiguration();
	    // We use a delta fitness evaluator because we compute a defect rate, not
	    // a point score!
	    // ----------------------------------------------------------------------
	    config.setAlwaysCaculateFitness(true);
	    config.setGPFitnessEvaluator(new DeltaGPFitnessEvaluator());
	    //config.setMinInitDepth(POP_MIN_DEPTH);//valor original 4
	    config.setMaxInitDepth(POP_MAX_DEPTH);//valor original 4
	    config.setPopulationSize(POP_SIZE);//valor original 1000
//		config.setRandomGenerator(new GaussianRandomGenerator());
	    config.setMaxCrossoverDepth(CROSSOVER_MAX_DEPTH);//valor original 8
	    config.setFitnessFunction(new MetricsGP3.FormulaFitnessFunctionOLD());
	    config.setStrictProgramCreation(true);
	    GPProblem problem = new MetricsGP3(config);
	    // Create the genotype of the problem, i.e., define the GP commands and
	    // terminals that can be used, and constrain the structure of the GP
	    // program.
	    // --------------------------------------------------------------------
	    GPGenotype gp = problem.create();
	    gp.setVerboseOutput(false);
	    // Start the computation with maximum 800 evolutions.
	    // if a satisfying result is found (fitness value almost 0), JGAP stops
	    // earlier automatically.
	    // --------------------------------------------------------------------
	    //gp.evolve(1000);//valor original 800

		System.out.println("crossover method: " + config.getCrossMethod() );
		System.out.println("crossover prob: " + config.getCrossoverProb());
		System.out.println("probability for dynamizing the arity of a node during growing a program: " + config.getDynamizeArityProb());
		System.out.println("probability that a function instead of a terminal is chosen in crossing over: " + config.getFunctionProb() );
		System.out.println("init strategy: " + config.getInitStrategy());
		System.out.println("max crossover depth: " + config.getMaxCrossoverDepth());
		System.out.println("max init depth: " + config.getMaxInitDepth());
		System.out.println("min init depth: " + config.getMinInitDepth());
		System.out.println("probability for mutation of a node during growing a program: " + config.getMutationProb());
		System.out.println("new chroms percent: " + config.getNewChromsPercent());
		System.out.println("reproduction prob: " + config.getReproductionProb());
		System.out.println("selection method: " + config.getSelectionMethod());


		for(int i = 0; i < GEN_QUANTITY; i++){//geracoes
	    	//getFuncError(gp.getGPPopulation().getGPPrograms());
	    	//if (i>0) System.out.println("before gen " + i + ": " + 100 * computeFuncError(gp.getAllTimeBest()) + ", Fitness: " + gp.getAllTimeBest().getFitnessValue());
	    	gp.evolve(1);

//	    	IGPProgram[] pop = gp.getGPPopulation().getGPPrograms();
//	    	for (int j = 0; j < gp.getGPPopulation().getPopSize(); j++) {
//	    		System.out.println("Fitness: " + pop[j].getFitnessValue() + ", " + pop[j].toStringNorm(0));
//			}

	    	//System.out.println("Gen " + i);
	    	//gp.outputSolution(gp.getAllTimeBest());
	    	//gp.outputSolution(gp.getFittestProgram());
	    	//gp.outputSolution(gp.getFittestProgramComputed());
	    	//System.out.println("after  gen " + i + ": " + 100 * computeFuncError(gp.getAllTimeBest()) + ", Fitness: " + gp.getAllTimeBest().getFitnessValue());
	    }

	    // Print the best solution so far to the console.
	    // ----------------------------------------------
	    gp.outputSolution(gp.getAllTimeBest());

	    System.out.println("Fitnes training: " + 100 * computeFuncError(gp.getAllTimeBest()));

	    if (PROGRAM < 1 || PROGRAM >7) {
			data = new LoadFiles("inputs/testSet" + TRAINING_SET + ".csv");
		} else {
			data = new LoadFiles("inputs/prog" + PROGRAM + "/testSet" + TRAINING_SET + ".csv");
		}
		nVer = data.getNVer();
		nLines = data.getNLines();
		iVer = data.getIVer();
		nTC = data.getNTC();
		tLines = data.getTLines();
		//Metrics = data.getMetrics();
		Spectra = data.getSpectra();
		faults = data.getfaults();
		funcError = new double[POP_SIZE];

		ArrayList<ArrayList<Double>> results = new ArrayList<ArrayList<Double>>();

		results.add(new ArrayList());
		results.get(0).add(100 * computeFuncError(gp.getAllTimeBest()));
		System.out.println("Fitnes test: " + results.get(0).get(0));
		results.add(testGPMetricPer(gp.getAllTimeBest()));
		results.add(testGPMetricLin(gp.getAllTimeBest()));
	    results.add(testGPMetric(gp.getAllTimeBest()));


	    // Create a graphical tree of the best solution's program and write it to
	    // a PNG file.
	    // ----------------------------------------------------------------------
	    problem.showTree(gp.getAllTimeBest(), "Metrics_best.png");


		return results;
  }

	private static ArrayList testGPMetric(final IGPProgram ind) {
		ArrayList results = new ArrayList();
		double acca1 = 0;
		double acca3 = 0;
		double acca5 = 0;
		double acca10 = 0;
		double wefa1 = 0;
		double wefa3 = 0;
		double wefa5 = 0;
		double wefa10 = 0;
		Object[] noargs = new Object[0];
		// Evaluate function for input numbers 0 to 20.
		// --------------------------------------------

		for (int i = 0; i < nVer; i++) {
			double result[][] = new double [nLines[i]][2];

			for (int j = 0; j < nLines[i]; j++) {
				//define o valor das variaveis M para o calculo da funcao fitness
				N.set(Spectra[i][0][j]);
				Ncf.set(Spectra[i][1][j]);
				Nuf.set(Spectra[i][2][j]);
				Ncs.set(Spectra[i][3][j]);
				Nus.set(Spectra[i][4][j]);
				Nc.set(Spectra[i][5][j]);
				Nu.set(Spectra[i][6][j]);
				Ns.set(Spectra[i][7][j]);
				Nf.set(Spectra[i][8][j]);
				try {
					// Execute the GP program representing the function to be evolved.
					// ----------------------------------------------------------------
					result[j][0] = j;
					result[j][1] = ind.execute_double(0, noargs);



				} catch (ArithmeticException ex) {
					// This should not4.0 +  happen, some illegal operation was executed.
					// ------------------------------------------------------------
					System.out.println("illegal operation");
					System.out.println(ind);
					throw ex;
		        }
			}
			acca1 += accan(1, result, i);//gets the number of faults located in the n first ranks
			acca3 += accan(3, result, i);
			acca5 += accan(5, result, i);
			acca10 += accan(10, result, i);
			wefa1 += wefan(1, result, i);
			wefa3 += wefan(3, result, i);
			wefa5 += wefan(5, result, i);
			wefa10 += wefan(10, result, i);

		}
		System.out.println("acc@1: " + acca1);
		System.out.println("acc@3: " + acca3);
		System.out.println("acc@5: " + acca5);
		System.out.println("acc@10: " + acca10);
		System.out.println("wef@1: " + wefa1);
		System.out.println("wef@3: " + wefa3);
		System.out.println("wef@5: " + wefa5);
		System.out.println("wef@10: " + wefa10);

		results.add(acca1);
		results.add(acca3);
		results.add(acca5);
		results.add(acca10);
		results.add(wefa1);
		results.add(wefa3);
		results.add(wefa5);
		results.add(wefa10);

		return results;
	}

	private static int accan(int n, double result[][], int ver) {
		int qtd = 0;//qtd of faulty elements in the top n rank positions

		//ordena o vetor com os resultados do individuo
		Arrays.sort(result, new Comparator<double[]>() {
			public int compare(double[] a, double[] b) {
				return Double.compare(b[1], a[1]);
			}
		});

		for (int i = 0; i < n && i < nLines[ver]; i++) {
			if (faults.get(ver).contains((int)result[i][0])) {
				qtd++;
			}
		}

		return qtd;
	}

	private static int wefan(int n, double result[][], int ver) {
		int qtd = 0;//quantity of non faulty elements in the rank befor reach a fault or the nth element

		//ordena o vetor com os resultados do individuo
		Arrays.sort(result, new Comparator<double[]>() {
			public int compare(double[] a, double[] b) {
				return Double.compare(b[1], a[1]);
			}
		});

		for (int i = 0; i < n && i < nLines[ver]; i++) {
			if (faults.get(ver).contains((int)result[i][0])) {
				break;//found a fault
			} else {
				qtd++;//did not found a fault
			}
		}

		return qtd;
	}








	/**
	 * proportion of located faults per instpected code ratio
	 * from 10% to 100% of code inspected
	 * shows percentage of located faults
	 * @param ind the individual
	 */
	private static ArrayList testGPMetricPer(final IGPProgram ind) {
		ArrayList results = new ArrayList();

        int nFaults = 0;
        for(int i = 0; i < faults.size(); i++) {
        	nFaults += faults.get(i).size();
        }
		Object[] noargs = new Object[0];

		double[] error = new double[10];
		for (int k = 0; k < 10; k++) {
			error[k] = 0;
			for (int i = 0; i < nVer; i++) {
	//			if (isInTraningSet(i)) {
					double result[][] = new double [nLines[i]][2];

					for (int j = 0; j < nLines[i]; j++) {
						//define o valor das variaveis M para o calculo da funcao fitness}
						N.set(Spectra[i][0][j]);
						Ncf.set(Spectra[i][1][j]);
						Nuf.set(Spectra[i][2][j]);
						Ncs.set(Spectra[i][3][j]);
						Nus.set(Spectra[i][4][j]);
						Nc.set(Spectra[i][5][j]);
						Nu.set(Spectra[i][6][j]);
						Ns.set(Spectra[i][7][j]);
						Nf.set(Spectra[i][8][j]);
						try {
							// Execute the GP program representing the function to be evolved.
							// ----------------------------------------------------------------
							result[j][0] = j;
							result[j][1] = ind.execute_double(0, noargs);



						} catch (ArithmeticException ex) {
							// This should not4.0 +  happen, some illegal operation was executed.
							// ------------------------------------------------------------
							System.out.println("illegal operation");
							System.out.println(ind);
							throw ex;
				        }
					}
					//error += getError(result, i);
					//System.out.println("Erro v" + i + ": " + getError(result, i));
					error[k] += qtdFaults(result, i, 0.1 * (k+1));
					//System.out.println( qtdFaults(result, i, 0.1));//getError(result, i));
	//			}
			}
		}
		//System.out.println("M" + M + ": Media = " + 100 * error/nVer + "%");
		String result = "";
		for (int i = 0; i < error.length; i++) {
			results.add(100 * error[i]/nFaults);
			result += String.valueOf(results.get(i)) + ";";
		}
		System.out.println("GP" + PROGRAM + "/" + TRAINING_SET + "(" + nFaults + ");" + result);

		return results;
	}

	/**
	 * proportion of located faults per number of inspected lines
	 * from 5 lines to 50 lines
	 * percentage of located faults with x lines of code investigated
	 * @param ind the individual
	 */
	private static ArrayList testGPMetricLin(final IGPProgram ind) {
		ArrayList results = new ArrayList();

        int nFaults = 0;
        for(int i = 0; i < faults.size(); i++) {
        	nFaults += faults.get(i).size();
        }
		Object[] noargs = new Object[0];

		double[] error = new double[10];
		for (int k = 0; k < 10; k++) {
			error[k] = 0;
			for (int i = 0; i < nVer; i++) {
	//			if (isInTraningSet(i)) {
					double result[][] = new double [nLines[i]][2];

					for (int j = 0; j < nLines[i]; j++) {
						//define o valor das variaveis M para o calculo da funcao fitness
						N.set(Spectra[i][0][j]);
						Ncf.set(Spectra[i][1][j]);
						Nuf.set(Spectra[i][2][j]);
						Ncs.set(Spectra[i][3][j]);
						Nus.set(Spectra[i][4][j]);
						Nc.set(Spectra[i][5][j]);
						Nu.set(Spectra[i][6][j]);
						Ns.set(Spectra[i][7][j]);
						Nf.set(Spectra[i][8][j]);
						try {
							// Execute the GP program representing the function to be evolved.
							// ----------------------------------------------------------------
							result[j][0] = j;
							result[j][1] = ind.execute_double(0, noargs);



						} catch (ArithmeticException ex) {
							// This should not4.0 +  happen, some illegal operation was executed.
							// ------------------------------------------------------------
							System.out.println("illegal operation");
							System.out.println(ind);
							throw ex;
				        }
					}
					//error += getError(result, i);
					//System.out.println("Erro v" + i + ": " + getError(result, i));
					error[k] += qtdFaultsLines(result, i, (k+1));
					//System.out.println( qtdFaults(result, i, 0.1));//getError(result, i));
	//			}
			}
		}
		//System.out.println("M" + M + ": Media = " + 100 * error/nVer + "%");
		String result = "";
		for (int i = 0; i < error.length; i++) {
			results.add(100 * error[i]/nFaults);
			result += String.valueOf(results.get(i)) + ";";
		}
		System.out.println("GP" + PROGRAM + "/" + TRAINING_SET + "(" + nFaults + ");" + result);

		return results;
	}

	private static int qtdFaultsLines(double result[][], int ver, double lines) {
		int qtd = 0;

		//ordena o vetor com os resultados do individuo
		Arrays.sort(result, new Comparator<double[]>() {
		    public int compare(double[] a, double[] b) {
		        return Double.compare(b[1], a[1]);
		    }
		});

		for (int i = 0; i < lines; i++) {
			if (faults.get(ver).contains((int)result[i][0])) {
				qtd++;
			}
		}

		return qtd;
	}

	private static int qtdFaults(double result[][], int ver, double proportion) {
		int qtd = 0;

		//ordena o vetor com os resultados do individuo
		Arrays.sort(result, new Comparator<double[]>() {
		    public int compare(double[] a, double[] b) {
		        return Double.compare(b[1], a[1]);
		    }
		});

		for (int i = 0; i < proportion*nLines[ver]; i++) {
			if (faults.get(ver).contains((int)result[i][0])) {
				qtd++;
			}
		}

		return qtd;
	}






	private static void getFuncError(IGPProgram[] programs) {
		for (int i = 0; i < programs.length; i++) {
			funcError[i] = computeFuncError(programs[i]);
		}
	}

	private static double computeFuncError(final IGPProgram ind) {
		double error = 0;
		Object[] noargs = new Object[0];
		// Evaluate function for input numbers 0 to 20.
		// --------------------------------------------

		for (int i = 0; i < nVer; i++) {
			double result[][] = new double [nLines[i]][2];

			for (int j = 0; j < nLines[i]; j++) {
				//define o valor das variaveis M para o calculo da funcao fitness
				N.set(Spectra[i][0][j]);
				Ncf.set(Spectra[i][1][j]);
				Nuf.set(Spectra[i][2][j]);
				Ncs.set(Spectra[i][3][j]);
				Nus.set(Spectra[i][4][j]);
				Nc.set(Spectra[i][5][j]);
				Nu.set(Spectra[i][6][j]);
				Ns.set(Spectra[i][7][j]);
				Nf.set(Spectra[i][8][j]);
				try {
					// Execute the GP program representing the function to be evolved.
					// ----------------------------------------------------------------
					result[j][0] = j;
					result[j][1] = ind.execute_double(0, noargs);



				} catch (ArithmeticException ex) {
					// This should not4.0 +  happen, some illegal operation was executed.
					// ------------------------------------------------------------
					System.out.println("illegal operation");
					System.out.println(ind);
					throw ex;
		        }
			}
			// O erro eh a prporcao de linhas necessarias para alcancar o defeito
			// -------------------------------------------------------------------
			error += getError(result, i)/nVer;

		}
		return error;///nVer;//tLines;
	}

	private static double getError(double result[][], int ver) {
		double error = 0;

		//ordena o vetor com os resultados do individuo
		Arrays.sort(result, new Comparator<double[]>() {
		    public int compare(double[] a, double[] b) {
		        return Double.compare(b[1], a[1]);
		    }
		});

		int nFaults = faults.get(ver).size();//qtd de falhas na versao
		for (int i = 0; i < nLines[ver]; i++) {
			error++;
			if (faults.get(ver).contains((int)result[i][0])) {
				nFaults--;//encontrou uma falha
			}
			if (i+1 < nLines[ver] && nFaults == 0 && result[i][1] != result[i+1][1])
				break;
		}

		return error/nLines[ver];//o valor deve ser poporcional a quantidade de linhas da versao
	}


	/*private static void testMetric() {
		double error = 0;
		Object[] noargs = new Object[0];

		for (int i = 0; i < nVer; i++) {
//			if (isInTraningSet(i)) {
				double result[][] = new double [nLines[i]][2];

				for (int j = 0; j < nLines[i]; j++) {
					result[j][0] = j;
					result[j][1] = Math.pow(Metrics[i][10][j], (((Math.pow(Metrics[i][7][j], Metrics[i][12][j]) - (Metrics[i][9][j] - Metrics[i][15][j])) * Metrics[i][1][j] * (Metrics[i][20][j] - Metrics[i][15][j])) - Metrics[i][10][j])) * Metrics[i][5][j] * Metrics[i][6][j];//Metrics[i][6][j] * Metrics[i][5][j] * (Math.sin(Math.sin(Math.pow(Metrics[i][5][j], ((Metrics[i][10][j] * Metrics[i][15][j] * Metrics[i][5][j]) - Math.pow(Metrics[i][5][j], (Metrics[i][10][j] - Metrics[i][10][j])))))));//Metrics[i][2][j] * Metrics[i][8][j] * ((Metrics[i][2][j] + (Metrics[i][2][j] + (Metrics[i][21][j] * Metrics[i][6][j]) + Metrics[i][2][j]) + (Metrics[i][2][j] * (Metrics[i][21][j] - Metrics[i][13][j]) * Metrics[i][21][j])) - Metrics[i][13][j]);
				}
				error += getError(result, i);
				//System.out.println("Erro v" + i + ": " + getError(result, i));
				//error += qtdFaults(result, i, 0.1);
				//System.out.println( qtdFaults(result, i, 0.1));//getError(result, i));
//			}
		}
		//System.out.println("Total = " + error);
		//System.out.println("Total = " + 100 * error/135.0 + "%");

		System.out.println("Media = " + 100 * error/nVer + "%");
		//System.out.println("Média = " + 100 * error/tLines + "%");
	}

	private static double getError(double result[][], int ver) {
		double error = 0;

		//ordena o vetor com os resultados do individuo
		java.util.Arrays.sort(result, new java.util.Comparator<double[]>() {
		    public int compare(double[] a, double[] b) {
		        return Double.compare(b[1], a[1]);
		    }
		});

		int nFaults = faults.get(ver).size();//qtd de falhas na versao
		for (int i = 0; i < nLines[ver]; i++) {
			error++;
			if (faults.get(ver).contains((int)result[i][0])) {
				nFaults--;//encontrou uma falha
			}
			if (i+1 < nLines[ver] && nFaults == 0 && result[i][1] != result[i+1][1])
				break;
		}


//		System.out.println("Ver: " + ver + ", nFault: " + faults.get(ver).size());
//
//		for (int i = 0; i < nLines[ver]; i++) {
//			if (faults.get(ver).contains((int)result[i][0])) {
//				System.out.println(result[i][0] + ", " + result[i][1] + " >> Fault");
//			} else {
//				if (i+1 < nLines[ver] && result[i][1] != result[i+1][1]) {
//					System.out.println(result[i][0] + ", " + result[i][1] + " Iguais");
//				} else {
//					System.out.println(result[i][0] + ", " + result[i][1]);
//				}
//			}
//		}
//
//		System.out.println("nLinhas investigadas: " + error);

		return error/nLines[ver];
	}

	private static int qtdFaults(double result[][], int ver, double proportion) {
		int qtd = 0;

		//ordena o vetor com os resultados do individuo
		java.util.Arrays.sort(result, new java.util.Comparator<double[]>() {
		    public int compare(double[] a, double[] b) {
		        return Double.compare(b[1], a[1]);
		    }
		});

		for (int i = 0; i < proportion*nLines[ver]; i++) {
			if (faults.get(ver).contains((int)result[i][0])) {
				for (int j = i; j < proportion*nLines[ver]; j++) {
					if (j+1 < proportion*nLines[ver] && result[j][1] != result[j+1][1]) {
						qtd++;
						break;
					}
				}
			}
		}

		return qtd;
	}*/

  /**
   * Fitness function for evaluating the produced fomulas, represented as GP
   * programs. The fitness is computed by calculating the result (Y) of the
   * function/formula for integer inputs 0 to 20 (X). The sum of the differences
   * between expected Y and actual Y is the fitness, the lower the better (as
   * it is a defect rate here).
   */
	public static class FormulaFitnessFunctionOLD extends GPFitnessFunction {
		protected double evaluate(final IGPProgram a_subject) {
			return computeRawFitness(a_subject);
    	}

		public double computeRawFitness(final IGPProgram ind) {
			double error = 0;
			Object[] noargs = new Object[0];
			// Evaluate function for input numbers 0 to 20.
			// --------------------------------------------

			for (int i = 0; i < nVer; i++) {
				double result[][] = new double [nLines[i]][2];

				for (int j = 0; j < nLines[i]; j++) {
					//define o valor das variaveis M para o calculo da funcao fitness
					N.set(Spectra[i][0][j]);
					Ncf.set(Spectra[i][1][j]);
					Nuf.set(Spectra[i][2][j]);
					Ncs.set(Spectra[i][3][j]);
					Nus.set(Spectra[i][4][j]);
					Nc.set(Spectra[i][5][j]);
					Nu.set(Spectra[i][6][j]);
					Ns.set(Spectra[i][7][j]);
					Nf.set(Spectra[i][8][j]);
					try {
						// Execute the GP program representing the function to be evolved.
						// ----------------------------------------------------------------
						result[j][0] = j;
						result[j][1] = ind.execute_double(0, noargs);



					} catch (ArithmeticException ex) {
						// This should not4.0 +  happen, some illegal operation was executed.
						// ------------------------------------------------------------
						System.out.println("illegal operation");
						System.out.println(ind);
						throw ex;
			        }
				}
				// O erro eh a prporcao de linhas necessarias para alcancar o defeito
				// -------------------------------------------------------------------
				error += getError(result, i);

			}
			return error;///nVer;//tLines;
		}

		private double getError(double result[][], int ver) {
			double error = 0;

			//ordena o vetor com os resultados do individuo
			Arrays.sort(result, new Comparator<double[]>() {
			    public int compare(double[] a, double[] b) {
			        return Double.compare(b[1], a[1]);
			    }
			});
						
			int nFaults = faults.get(ver).size();//qtd de falhas na versao
			for (int i = 0; i < nLines[ver]; i++) {
				error++;
				if (faults.get(ver).contains((int)result[i][0])) {
					nFaults--;//encontrou uma falha
				}
				if (i+1 < nLines[ver] && nFaults == 0 && result[i][1] != result[i+1][1])
					break;
			}
			
			return 100*error/nLines[ver];//o valor deve ser poporcional a quantidade de linhas da versao
		}
	}
}